


    window.addEventListener("load", (event) => {
        document.querySelector('body').classList.add('loaded')
      });



    document.querySelectorAll('.js_dashboard_content__info__view button').forEach(el=>{
        el.addEventListener('click', function(e){
            e.preventDefault()
            if (this.classList.contains('active')) return false;

            document.querySelector('.js_dashboard_content__info__view button.active').classList.remove('active')
            this.classList.add('active')
            document.querySelector('.js_dashboard_content__list_x').classList.toggle('list')
        })
    })


    document.querySelectorAll('.js_header__btn_menu,.js_btn_close_menu,.js_overlay_menu').forEach(el=>{
        el.addEventListener('click', function(e){
            e.preventDefault()
            document.querySelector('.js_header__btn_menu').classList.toggle('active')
            document.querySelector('.js_modal_menu').classList.toggle('active')
        })
    })


    document.querySelectorAll('.js_dashboard__setting__val__edit').forEach(el=>{
        el.addEventListener('click', function(e){
            e.preventDefault();
            this.style.display = 'none';

            this.closest('.js_dashboard__setting__block__list').querySelectorAll('input.field_edit').forEach(el=>{
                el.removeAttribute('disabled')
            });

            this.closest('.dashboard__setting__val__btns').querySelector('.js_dashboard__setting__val__save').style.display = 'flex';
        })
    })


    document.querySelectorAll('.js_dashboard__setting__val__save').forEach(el=>{
        el.addEventListener('click', function(e){
            e.preventDefault();
            this.style.display = 'none';

            this.closest('.js_dashboard__setting__block__list').querySelectorAll('input.field_edit').forEach(el=>{
                el.setAttribute('disabled','disabled')
            });

            this.closest('.dashboard__setting__val__btns').querySelector('.js_dashboard__setting__val__edit').style.display = 'flex';
        })
    })