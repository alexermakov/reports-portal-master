'use strict';

const sessionMaxAge = 25 * 60 * 1000;

let time;
let keepAlive = false;

// Redirect to login after prolonged idleness.
const handleIdleness = () => {
  window.onload = resetTimer;

  // DOM Events
  window.onmousemove = resetTimer;
  window.onmousedown = resetTimer; // Catches touchscreen presses.
  window.ontouchstart = resetTimer; // Catches touchscreen swipes.
  window.ontouchmove = resetTimer; // Required by some devices.
  window.onclick = resetTimer; // Catches touchpad clicks.
  window.onkeydown = resetTimer;
  window.addEventListener('scroll', resetTimer, true); // Catch during capture phase, since scroll events don't bubble.
};

// Keep session alive while working inside iFrame.
const handleKeepAlive = () => {

  setInterval(() => {

    if (keepAlive) {
      fetch('/session/keep-alive')
        .then(response => {
          if (response.status === 401) {
            redirectToLogin();
          } else {
            keepAlive = false;

            resetTimer();
          }
        });
    }
  }, sessionMaxAge - 5000); // Subtract 5 seconds to account for timer discrepancies.
};

const resetTimer = (event) => {
  keepAlive = !! event;

  clearTimeout(time);
  time = setTimeout(redirectToLogin, sessionMaxAge + 5000); // Add 5 seconds to account for timer discrepancies.
}

const redirectToLogin = () => {
  window.location.href = '/login';
};

if (! window.location.href.endsWith('/login')) {
  handleIdleness();
}

if (window.location.href.endsWith('/reports/power-bi')) {
  handleKeepAlive();
}
