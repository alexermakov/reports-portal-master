'use strict';

(function() {
  var view = document.querySelectorAll('.view-pwd');

  var _loop = function _loop(i) {
    var icon = view[i].querySelector('.icon');

    view[i].onclick = function() {
      var input = this.nextElementSibling;

      if (input.type === 'password' && icon.classList.contains('icon--eye')) {
        input.type = 'text';
        icon.classList.remove('icon--eye');
        icon.classList.add('icon--eye-invisible');
      } else {
        input.type = 'password';
        icon.classList.remove('icon--eye-invisible');
        icon.classList.add('icon--eye');
      }
    };
  };
  for (var i = 0; i < view.length; i++) {
    _loop(i);
  }
})();

var form = document.querySelector('#form');

if (form) {
  var userFirstName = form.querySelector('input[id="firstName"]');
  var userLastName = form.querySelector('input[id="lastName"]');
  var userEmail = form.querySelector('input[id="email"]');
  var userPassword = form.querySelector('input[id="password"]');
  var userNewPassword = form.querySelector('input[id="newPassword"]');
  var userPasswordConfirm = form.querySelector('input[id="passwordConfirm"]');
  var passcode = form.querySelector('input[id="passcode"]');
  var phoneNumber = form.querySelector('input[id="phone-number"]');
  var phoneNumberParts = form.querySelector('span[id="phone-number-parts"]');
  var contactMethod = form.querySelector('span[id="contact-method"]');

  form.addEventListener('change', function(event) {
    if (userFirstName) {
      validateTextField(userFirstName, 'First Name cannot be blank');
    }

    if (userLastName) {
      validateTextField(userLastName, 'Last Name cannot be blank');
    }

    if (userEmail) {
      validateEmail(userEmail);
    }

    if (userPassword) {
      validatePasswordField(userPassword);
    }

    if (passcode) {
      validatePasscodeField(passcode);
    }

    if (phoneNumberParts) {
      validatePhoneNumberParts(phoneNumberParts);
    }

    if (contactMethod) {
      validateContactMethod(contactMethod);
    }
  });

  if (userNewPassword) {
    userNewPassword.onkeyup = function() {
      validateNewPassword(userNewPassword);
      validatePasswordField(userNewPassword);
    };
  }

  if (userPasswordConfirm) {
    userPasswordConfirm.onkeyup = function() {
      validatePasswordConfirm(userPasswordConfirm);
    };
  }

  function errorFieldCreate(elem, text) {
    errorFieldRemove(elem);

    elem.classList.add('form-error');

    var inputElems = elem.querySelectorAll('input');

    for (var inputElem of inputElems) {
      inputElem.classList.add('form-error');
    }

    elem.insertAdjacentHTML(
      'afterend',
      '<p class="form-error-field helper-text">'.concat(text, '</p>'),
    );
  }

  function errorFieldRemove(elem) {
    var error = elem.nextElementSibling;

    if (error && elem.classList.contains('form-error')) {
      elem.classList.remove('form-error');
      error.remove();
    }

    var inputElems = elem.querySelectorAll('input');

    for (var inputElem of inputElems) {
      inputElem.classList.remove('form-error');
    }
  }

  function validateTextField(item, text) {
    errorFieldRemove(item);

    if (! item.value) {
      errorFieldCreate(item, text);
    }
  }

  function validateEmail(item) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    errorFieldRemove(item);

    if (! reg.test(item.value)) {
      errorFieldCreate(item, 'Email address is invalid');
    }

    if (! item.value) {
      errorFieldCreate(item, 'Email cannot be blank');
    }

    if (item.value.length > 255) {
      errorFieldCreate(item, 'Email should contain at most 255 characters');
    }

    const disableLtcUsers = item.classList.contains('disable-ltc-users');
    const ltcDomain = /ltc(ally|cs)\.com/.test(item.value.split('@').pop());

    if (disableLtcUsers && ltcDomain) {
      errorFieldCreate(item, 'Please login with your LTC account');
    }
  }

  function validatePasswordField(item) {
    var reg = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*[!-\/:-@\[-`{-~]).{8,256}$/;

    errorFieldRemove(item);

    if (! reg.test(item.value)) {
      errorFieldCreate(
        item,
        'Password should contain at least 8 chars, and at least one of each: uppercase, lowercase, digit and special character',
      );
    }
  }

  function validateNewPassword(item) {
    errorFieldRemove(item);

    var val = item.value;

    if (userPasswordConfirm.value === val && userPasswordConfirm.nextSibling) {
      errorFieldRemove(userPasswordConfirm);
    } else if (userPasswordConfirm.value && userPasswordConfirm.value !== val) {
      errorFieldCreate(
        userPasswordConfirm,
        'Confirm New Password must be equal to New Password',
      );
    }
  }

  function validatePasswordConfirm(item) {
    errorFieldRemove(item);

    var val = item.value;

    if (userNewPassword.value !== val) {
      errorFieldCreate(
        item,
        'Confirm New Password must be equal to New Password',
      );
    }
  }

  function validatePasscodeField(item) {
    var reg = /^[0-9]{6}$/;

    errorFieldRemove(item);

    if (! reg.test(item.value)) {
      errorFieldCreate(item, 'Passcode is invalid');
    }
  }

  function validatePhoneNumberParts(item) {
    errorFieldRemove(item);

    var inputs = phoneNumberParts.querySelectorAll('input');

    for (var input of inputs) {
      if (input.value.length !== input.maxLength) {
        errorFieldCreate(item, 'Phone number is invalid');

        return;
      }
    }
  }

  function validateContactMethod(item) {

    errorFieldRemove(item);

    var selected = item.querySelector('input:checked');

    if (! selected) {
      errorFieldCreate(item, 'Select passcode delivery method');
    }
  }

  (function() {
    form.onsubmit = function(e) {
      if (userFirstName) {
        validateTextField(userFirstName, 'First Name cannot be blank');
      }

      if (userLastName) {
        validateTextField(userLastName, 'First Name cannot be blank');
      }

      if (userEmail) {
        validateEmail(userEmail);
      }

      if (userPassword) {
        validatePasswordField(userPassword);
      }

      if (userNewPassword) {
        validateNewPassword(userNewPassword);
        validatePasswordField(userNewPassword);
      }

      if (userPasswordConfirm) {
        validatePasswordConfirm(userPasswordConfirm);
      }

      if (passcode) {
        validatePasscodeField(passcode);
      }

      if (phoneNumberParts) {
        validatePhoneNumberParts(phoneNumberParts);
      }

      if (contactMethod) {
        validateContactMethod(contactMethod);
      }

      var errors = form.querySelectorAll("input.form-error");

      if (errors.length) {
        e.preventDefault();
      }
    };
  })();

  (function() {
    if (phoneNumberParts) {
      phoneNumberParts.oninput = function(e) {
        var target = e.target;
        var value = target.value;
        var maxLength = target.maxLength;
        var nextSibling = target.nextSibling;

        if (value.length === maxLength && nextSibling) {
          nextSibling.focus();
        }

        if (value.length > maxLength) {
          target.value = value.slice(0, maxLength);
        }

        phoneNumber.value = '';

        var inputs = phoneNumberParts.querySelectorAll('input');

        for (var input of inputs) {
          phoneNumber.value += input.value;
        }
      };
    }
  })();
}
