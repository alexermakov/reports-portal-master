const { service, factories, models } = require('powerbi-client');

const fetchReports = async (route) => {
  const response = await fetch(route);
  return await response.json();
};

// Get a reference to the embedded report HTML element
const embedContainer = document.getElementById('elementId');

if (embedContainer) {

  fetchReports('/reports/power-bi/embed')
    .then(data => {
      const {
        data: {
          groupId,
          reportId,
          embedToken,
        },
      } = data;

      const embedUrl = `https://app.powerbi.com/reportEmbed?reportId=${reportId}&groupId=${groupId}`;
      const powerBi = new service.Service(factories.hpmFactory, factories.wpmpFactory, factories.routerFactory);
      const orientation = screen.orientation.type.startsWith('portrait') ? 'portrait' : 'landscape';
      const mobileDevice = (orientation === 'portrait') ? window.innerWidth <= 480 : window.innerHeight <= 480;

      let layout;

      if (mobileDevice && orientation === 'portrait') {
        layout = models.LayoutType.MobilePortrait;
      } else if (mobileDevice && orientation === 'landscape') {
        layout = models.LayoutType.MobileLandscape;
      } else {
        layout = models.LayoutType.Master;
      }

      const config = {
        type: 'report',
        tokenType: models.TokenType.Embed,
        accessToken: embedToken,
        embedUrl: embedUrl,
        id: reportId,
        permissions: models.Permissions.Read,
        settings: {
          panes: {
            filters: {
              visible: false,
            },
            pageNavigation: {
              visible: false,
            },
          },
          layoutType: layout,
        },
      };

      // Embed the report and display it within the div container.
      powerBi.embed(embedContainer, config);

    }).catch((err) => {
    console.error(err);
  });
}
