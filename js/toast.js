const toast = document.querySelector('#toast');

if (toast) {
  setTimeout(function() {
    toast.remove();
  }, 5000);
}
